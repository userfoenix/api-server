var path = require('path');
var webpack = require('webpack');

module.exports = {
	entry:  './app/index.jsx',
	output: {
		path: __dirname+'/public',
		filename: 'bundle.js',
		library: 'App'
	},
	module: {
		loaders: [
			{
				test: /\.css$/,
				loader: "style-loader!css-loader"
			},
			{
				test: /.jsx?$/,
				loader: 'babel',
				exclude: /node_modules|bower_components/
			}
		]
	}
};
