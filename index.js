const express = require('express');
const app = express();
const http = require('http').Server(app);
const io = require('socket.io')(http);
const Command = require('./lib/commands');
const API = require('./lib/api');
const logger = require('./lib/logger');
const SessionClass = require('./lib/session');

app.set('views', './public'); // specify the views directory
app.use('/', express.static('public'));
app.use('/bower', express.static('bower_components'));

app.set('view engine', 'ejs');
app.get('/', function(req, res){
	var guid = +req.query.guid;
	res.render('index', {guid:guid,username:guid});
});

const Session = new SessionClass();
Command.init(Session);

// Socket.io layer
io.on('connection', function(socket){
	var query = socket.handshake.query,
		token = query.token,
		guid = query.guid;

	// Error. Invalid token
	if (!token || !guid){
		logger.log('Invalid Args', query);
		socket.disconnect();
		return;
	}
	// ---

	if (!Session.findByToken(token)){
		Session.add(token, {guid:guid, socket:socket, username:query.username});
	}

	// Send my online status to friends
	Command.sendToFriends(guid, new API.Response('status', {guid:guid, online:1}));

	// Send me who is online
	var aStatus = Session.getOnlineFriendsGuids(guid).map(guid => {
		return {guid:+guid, online:1};
	});
	if (aStatus.length) {
		socket.emit('status', new API.Response('status', aStatus));
	}
	// ---

	// Handle api commands
	socket.on('init', o => Command.execute(guid,'init',o));
	socket.on('msg', o => Command.execute(guid,'msg',o));
	socket.on('typing', o => Command.execute(guid,'typing',o));
	// ---

	// Handle disconnect
	socket.on('disconnect', function(){
		var ses = Session.remove(token);
		if (!ses){
			return;
		}
		var own_sessions = Session.findByGuid(guid);
		if (!own_sessions.length) {
			// Send offline for friends if last user session exit
			Command.sendToFriends(guid, new API.Response('status',{guid:guid, online:0}));
		}
	});
	// ---
});
// ---

// Http layer
http.listen(3000, function(){
	logger.log('listening on *:3000');
});
// ---