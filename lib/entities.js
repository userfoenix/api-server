'use strict';

const uInt64 = require('./uint64');

class Correspondent {
	/**
	 * Construct correspondent
	 * @param {number|string} id Guid or GroupId
	 * @param {number} [is_grp]
	 */
	constructor(id, is_grp=0){
		if (typeof(id)=='object' && (id instanceof Correspondent)){
			return id;
		}
		this.is_grp = +is_grp;
		if (typeof(id)=='string' && id.indexOf('#')===0){
			id = id.slice(1);
			this.is_grp = 1;
		}
		this.id = parseInt(id,10) || 0;
		if (this.id<=0){
			throw new TypeError('Invalid id!');
		}
	}
	toString(){
		return (this.is_grp ? '#' : '')+this.id;
	}
}

class MsgEntity {
	/**
	 * Create message
	 * @param {Correspondent} from
	 * @param {string} text
	 * @param {string} atime
	 * @param {number} [flags]
	 * @param {object} [attach]
	 */
	constructor(from,text,atime,flags=0,attach=null){
		this.from = from;
		this.text = text;
		this.atime = atime;
		this.flags = parseInt(flags,10) || 0;
		this.attach = attach;
	}
	toJSON(){
		let result = {
			from: this.from.toString(),
			text: this.text,
			atime: this.atime,
			flags: this.flags
		};
		if (this.attach && typeof(this.attach)=='object'){
			result.attach = this.attach;
		}
		return result;
	}
	static randomMsg(from){
		from = from || Math.floor(Math.random()*100);
		let tm = (Date.now()/1000)^0;
		let text = `Test text ${from} - ${tm}`;
		let atime = new uInt64(0,tm/100);
		return new MsgEntity(from,text,atime.toString(),0);
	}
}

class ContactEntity {
	/**
	 * @constructor
	 * @param {number|string} guid User guid
	 * @param {string} name User full name, depends on permission
	 * @param {string} [pht] Path to photo
	 * @param {number} [online] 0-off,1-on
	 * @param {number} [gender] Gender 1-male, 2-female
	 * @param {string} [status] Text status
	 */
	constructor(guid,name,pht='',online=0,gender=0,status=''){
		if (!guid || !name){
			throw new TypeError('Invalid args!',arguments);
		}
		this.guid = ''+guid;
		this.name = name;
		this.pht = pht;
		this.online = +online;
		this.gender = +gender;
		this.status = status;
	}
	toJSON(){
		let result = {guid:''+this.guid, name:this.name, pht:this.pht};
		if (this.online){ result.online = this.online; }
		if (this.gender){ result.gender = this.gender; }
		if (this.status){ result.status = this.status; }
		return result;
	}
}

class GroupEntity {
	/**
	 * @constructor
	 * @param {number} id global group ID
	 * @param {string} name group name
	 * @param {string} [avatar] Path to avatar
	 * @param {number} [count] Members count (cache)
	 * @param {number} [type] normal,public,etc.. - default 0
	 */
	constructor(id,name,avatar='',count=0,type=0){
		if (!id || !name){
			throw new TypeError('Invalid args!',arguments);
		}
		this.id = +id;
		this.name = name;
		this.avatar = avatar;
		this.count = count;
		this.type = type;
	}
	toJSON(){
		let result = {id:''+this.id, name:this.name, avatar:this.avatar};
		if (this.count){ result.count = this.count; }
		if (this.type){ result.type = this.type; }
		return result;
	}
}

class RosterEntity {
	/**
	 * @constructor
	 * @param {Array} [items] [{corr:corr,modtime:modtime},..]
	 */
	constructor(items){
		this.items = {};
		if (items && Array.isArray(items)){
			items.forEach(o=>this.add(o.corr,o.modtime),this);
		}
	}
	/**
	 * Add/update roster item
	 * @param {Correspondent|string} corr
	 * @param {number} [modtime] Last modified time
	 */
	add(corr,modtime=0){
		if (!corr) {
			return;
		}
		if (!(corr instanceof Correspondent)) {
			corr = new Correspondent(corr);
		}
		this.items[corr] = {modtime: parseInt(modtime,10) || 0 }
	}
	toJSON(){
		var result = [];
		for (let corr in this.items){
			let o = this.items[corr];
			result.push({corr:corr,modtime:o.modtime});
		}
		result.sort((a,b) => {return a.modtime-b.modtime;});
		return result;
	}
}
module.exports = {
	Correspondent : Correspondent,
	Message: MsgEntity,
	Roster: RosterEntity,
	Contact: ContactEntity,
	Group: GroupEntity
};
