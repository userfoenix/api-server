class Store {
	constructor(items){
		this.items = [];
		if (items && Array.isArray(items)){
			items.forEach(this.add,this);
		}
	}
	add(item){
		this.items.push(item);
	}
	getAll(){
		return this.items;
	}
	getBy(field,val){
		if (!field){
			return null;
		}
		let res = this.items.filter(u => u[field]===val);
		if (!res.length){
			return null;
		}
		return res[0];
	}
	getById(id){
		return this.getBy('guid',id);
	}
}

const Users = new Store([
	{guid:1, login:'user1', name:'User 1', email:'user1@gmail.com'},
	{guid:2, login:'user2', name:'User 2', email:'user2@gmail.com'},
	{guid:3, login:'user3', name:'User 3', email:'user3@gmail.com'},
	{guid:4, login:'user4', name:'User 4', email:'user4@gmail.com'},
	{guid:5, login:'user5', name:'User 5', email:'user5@gmail.com'}
]);

const Groups = new Store([
	{id:1, name:'Test Group 1', count: 5},
	{id:2, name:'Test Group 2', count: 5},
	{id:3, name:'Test Group 3', count: 5}
]);

const Friends = new Store([
	{guid:1, friends:[1,2,3,4,5]},
	{guid:2, friends:[1,2,3,4,5]},
	{guid:3, friends:[1,2,3,4,5]},
	{guid:4, friends:[1,2,3,4,5]},
	{guid:5, friends:[1,2,3,4,5]}
]);
Friends.getFriends = function(guid){
	let item = this.getById(guid);
	return item ? item.friends : [];
};

module.exports = {
	Users: Users,
	Groups: Groups,
	Friends: Friends
};