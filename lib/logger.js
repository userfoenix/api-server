const winston = require('winston');

const Logger = new (winston.Logger)({
	transports: [
		new (winston.transports.Console)({
			colorize: true,
			json: true}),
		new (winston.transports.File)({
			filename: 'debug.log',
			level: 'info',
			json: true
		})
	]
});

module.exports = Logger;

