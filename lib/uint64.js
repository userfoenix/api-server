'use strict';

/**
 * Create unsigned Int64 object fron array, aruguments list or string
 * Example:
 *  o = uInt64(); => uInt64#{hi=0, lo=0}
 *  o = uInt64(32bit); => uInt64#{hi=32bit, lo=0}
 *  o = uInt64(32bit,32bit); => uInt64#{hi=32bit, lo=32bit}
 *  o = uInt64([32bit,32bit]); => uInt64#{hi=32bit, lo=32bit}
 *  o = uInt64([32bit]); => => uInt64#{hi=32bit, lo=0}
 *  o = uInt64(NaN|null, *); => uInt64#{hi=NaN, lo=NaN}
 *  o = uInt64(*, NaN|null); => uInt64#{hi=NaN, lo=NaN}
 *  o = uInt64.parse("64bit integer as string", base); => uInt64#{hi=32bit, lo=32bit}
 * @param a
 * @param [b]
 * @returns {uInt64}
 */
function uInt64(a, b){
	var lo=a, hi=b;
	if (Array.isArray(a)){
		lo = a[0];
		hi = a[1];
	}
	var isUndef = typeof(hi)==='undefined';
	lo = lo===null ? NaN : Number(lo);
	hi = hi===null ? NaN: Number(hi);
	if (isNaN(lo) || lo<0 || lo>=uInt64.MAX32){
		this.low = NaN;
		this.high = NaN;
	}
	else{
		if (isNaN(hi) || hi<0 || hi>=uInt64.MAX32){
			this.high = isUndef ? 0 : NaN;
			this.low = isUndef ? lo : NaN;
		}
		else{
			this.low = lo;
			this.high = hi;
		}
	}
}

uInt64.DICT_STR = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
uInt64.DICT = uInt64.DICT_STR.split('');

uInt64.MAX16 = 65536;
uInt64.MAX32 = 4294967296;
uInt64.MAX_VALUE = uInt64.MAX32-1;
uInt64.MIN_VALUE = 0;

/**
 * Parse string in the specified base
 * @param str
 * @param [base] Allowed base 2..36
 * @returns {uInt64}
 */
uInt64.parse = function(str, base){
	if (typeof(str)=='object' && (str instanceof uInt64)){
		return str;
	}
	if (typeof(str)!=='string'){
		// uInt64.parse: The first argument must be string
		return uInt64.NaN;
	}

	base = base || 10;
	if (typeof(base)!='number' || base<2 || base>36){
		throw "uInt64. Base must be an integer at least 2 and no greater than 36";
	}

	str = str.toUpperCase(); // Capitalize string
	var arr = [0,0];
	for (var i=0,exceed=0,val,d; i<str.length; i++){
		val = uInt64.DICT_STR.indexOf(str[i]);
		if (val==-1 || val>=base){
			// uInt64.parse: Illegal symbol detected
			return uInt64.NaN;
		}
		arr[0] = arr[0]*base + val;
		arr[1] = arr[1]*base;
		if (arr[0]>=uInt64.MAX32){
			exceed = 1;
		}
		if (exceed){
			d = Math.floor(arr[0]/uInt64.MAX32);
			arr[0] -= d*uInt64.MAX32;
			arr[1] += d;
		}
		if (arr[1]>=uInt64.MAX32){
			// uInt64.parse: Number exceeds 2^64-1
			return uInt64.NaN;
		}
	}

	return new uInt64(arr);
};

/**
 * Cast cal to int64
 * @param val
 * @param {uInt64} [defInt64] Default value
 * @return {uInt64}
 */
uInt64.cast = function(val, defInt64){
	var result = null;
	if (val instanceof uInt64){
		result = val;
	}
	else if (typeof(val)=='string'){
		result = uInt64.parse(val,10);
	}
	else if ($.isArray(val) && val.length==2){
		result = new uInt64(val);
	}
	if (!result){
		result = (defInt64 instanceof uInt64) ? defInt64 : uInt64.NaN;
	}
	if ((defInt64 instanceof uInt64) && result.isNaN()){
		result = defInt64;
	}
	return result;
};

/**
 * Compare 64bit integer. Varialbles is not instanced of uInt64 will cast to uInt64
 * @param aa
 * @param bb
 * @returns {number}
 */
uInt64.compare = function(aa, bb){
	var a=uInt64.cast(aa), b=uInt64.cast(bb);
	if (a.isNaN() || b.isNaN()){
		//_log('uInt64.compre: One or both arguments isNaN!');
		return 0; // comapre with NaN always returns FALSE
	}
	if (a.high>b.high) return 1;
	if (a.high<b.high) return -1;
	if (a.low>b.low) return 1;
	if (a.low<b.low) return -1;
	return 0;
};

/**
 * Check longint variables is eqaul. Varialbles is not instanced of uInt64 will cast to uInt64
 * @param aa
 * @param bb
 * @returns {boolean}
 */
uInt64.equal = function(aa,bb){
	var a=uInt64.cast(aa), b=uInt64.cast(bb);
	if (a.isNaN() || b.isNaN()) return false; // comapre with NaN always returns FALSE
	return (a.high==b.high && a.low==b.low);
};

/**
 * Returns max of arguments or max from array. Elements is not instanced of uInt64 will cast to uInt64
 * Example:
 *  max = uInt64.max(a,b,c);
 *  max = uInt64.max([a,b,c]);
 * @returns {uInt64}
 */
uInt64.max = function(){
	if (!arguments.length) return null;
	var list = (arguments.length==1 && $.isArray(arguments[0])) ? arguments[0] : arguments,
		hasNaN = 0;
	for (var i=0,item,result; i<list.length; i++){
		item = uInt64.cast(list[i]);
		if (item.isNaN()) {
			hasNaN = 1; break;
		}
		if (!result || uInt64.compare(item, result)>0) {
			result = item;
		}
	}
	if (hasNaN || typeof(result)=='undefined'){
		return uInt64.NaN;
	}
	return result;
};

/**
 * Returns max of arguments or max from array. Elements is not instanced of uInt64 will cast to uInt64
 * Example:
 *  max = uInt64.min(a,b,c);
 *  max = uInt64.min([a,b,c]);
 * @returns {uInt64}
 */
uInt64.min = function(){
	if (!arguments.length) return null;
	var list = (arguments.length==1 && $.isArray(arguments[0])) ? arguments[0] : arguments,
		hasNaN = 0;
	for (var i=0,item,result; i<list.length; i++){
		item = uInt64.cast(list[i]);
		if (item.isNaN()) {
			hasNaN = 1; break;
		}
		if (!result || uInt64.compare(item, result)<0) {
			result = item;
		}
	}
	if (hasNaN || typeof(result)=='undefined'){
		return uInt64.NaN;
	}
	return result;
};

/**
 * Compress uInt64 to the base 36 string
 * @param val
 * @return {string}
 */
uInt64.compress = function(val){
	if (!val){ return ''; }
	var u = uInt64.cast(val);
	return u.isNaN() ? '' : u.compress();
};

/**
 * Check val is uInt64.NaN
 * @param val
 * @return {*}
 */
uInt64.isNaN = function(val){
	if (!val) return true;
	val = uInt64.cast(val);
	if (val instanceof uInt64){
		return val.isNaN();
	}
	return true;
};

/** Ptotottype */
uInt64.prototype = {
	/**
	 * Check bitmask is true/false
	 * @param x Bitmask
	 * @returns {boolean}
	 */
	test: function(x){
		if (typeof(x)==='number') {
			return (this.low && (this.low & x)!==0) ? true : false;
		}
		var int64=this.and(x);
		if (int64.isNaN()) return false;
		return int64.low!==0;
	},
	/**
	 * Binary AND
	 * @param x Bitmask
	 * @returns {*}
	 */
	and: function(x){
		var type = $.type(x);
		if (type!=='object' && type!=='number'){
			return uInt64.NaN;
		}
		var lo=this.low,
			hi=this.high;
		if (type==='object' && (x instanceof uInt64)){
			lo &= x.low;
			hi &= x.high;
		}
		else{
			lo &= x;
		}
		if (lo<0){ lo += uInt64.MAX32; }
		if (hi<0){ hi += uInt64.MAX32; }
		return new uInt64(lo, hi);
	},
	/**
	 * Binary OR
	 * @param x Bitmask
	 * @returns {uInt64}
	 */
	or: function(x){
		var type = $.type(x);
		if (type!=='object' && type!=='number'){
			return uInt64.NaN;
		}
		var lo=this.low, hi=this.high;
		if (type==='object' && (x instanceof uInt64)){
			lo |= x.low;
			hi |= x.high;
		}
		else{
			lo |= x;
		}
		if (lo<0){ lo += uInt64.MAX32; }
		if (hi<0){ hi += uInt64.MAX32; }
		return new uInt64(lo, hi);
	},
	/**
	 * Convert uInt64 to string with specified base
	 * @param {number} [base] In range 2..36
	 * @returns {string}
	 */
	toString: function(base){
		base = base || 10;
		if (typeof(base)!='number' || base<2 || base>36){
			throw "uInt64. Base must be an integer at least 2 and no greater than 36";
		}
		if (this.isNaN()){
			return '';
		}
		var aInt = this.toArray(),
			s='', aTemp, nonzero, reminder, t1, _hi, _lo, t;
		do {
			nonzero = -1;
			reminder = 0; //current reminder,
			aTemp = []; // temp
			for (var i=aInt.length-1;i>=0;i--) {
				_hi = aInt[i]>>>16; // aInt[i] high bits
				_lo = aInt[i]-_hi*uInt64.MAX16; // aInt[i] low bits (x & 0xFFFF)
				t = reminder*uInt64.MAX16+_hi;
				t1 = (t % base)*uInt64.MAX16;
				aTemp[i] = Math.floor((t1+_lo)/base) + Math.floor(t/base)*uInt64.MAX16;
				reminder = (t1+_lo) % base;
				if (nonzero==-1 && aTemp[i]!=0){
					nonzero = i;
				}
			}
			if (!nonzero){
				aTemp.splice(1,1);
			}
			s = uInt64.DICT[reminder]+s;
			aInt = aTemp;
		} while(nonzero>=0);
		return s;
	},
	/** Object to array */
	toArray: function(){
		return [this.low, this.high];
	},
	/**
	 * How to convert uInt64 to json
	 * @returns {Array}
	 */
	toJSON: function(){
		return [this.low, this.high];
	},
	/**
	 * Returns this.high as primitive value
	 * @returns {Number}
	 */
	valueOf: function(){
		return this.low;
	},
	/**
	 * Check uInt64 is zero
	 * @returns {boolean}
	 */
	isZero: function(){
		return this.low==0 && this.high==0;
	},
	/**
	 * Check uInt64 is NaN
	 * @returns {boolean}
	 */
	isNaN: function(){
		return isNaN(this.low) || isNaN(this.high);
	},
	/**
	 * Compress instance to the base 36 string
	 * @return {string}
	 */
	compress: function(){
		return this.toString(36).toLowerCase();
	}
};

/**
 * NaN uInt64 object
 * @type {uInt64}
 */
uInt64.NaN = new uInt64(NaN, NaN);

/**
 * Zero uInt64 object
 * @type {uInt64}
 */
uInt64.ZERO = new uInt64(0, 0);



module.exports = uInt64;