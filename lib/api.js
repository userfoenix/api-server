/**
 * Created by SERG on 16.06.2016.
 */
/**
 * Response to client from server
 */
class Response {
	/**
	 * @constructor
	 * @param {string} cmd
	 * @param {object} [result]
	 * @param {object} [error]
	 */
	constructor(cmd, result, error) {
		this.cmd = cmd;
		if (result) {
			this.result = result;
		}
		if (error) {
			this.error = error;
		}
		this.req_id = null;
	}
	get payload(){
		var res = {};
		if (this.req_id){
			res.req_id = this.req_id;
		}
		if (this.error){
			res.error = this.error;
		}
		else{
			res.result = this.result;
		}
		return res;
	}
}
class ErrorResponse extends Response {
	/**
	 * @constructor
	 * @param {string} cmd
	 * @param {number} code Error code
	 * @param {string} reason Error reason
	 */
	constructor(cmd,code,reason){
		super(cmd, null, {code:code, reason:reason});
	}
}

module.exports = {
	Response: Response,
	ErrorResponse: ErrorResponse
};