const API = require('./api');
const uInt64 = require('./uint64');
const Entites = require('./entities');
const Data = require('./store');
const logger = require('./logger');

/**
 * Emulate externa command to the unet cluster
 * @type {object}
 */
const ExtCmd = {
	messages: function(payload){
		let from = payload.from;
		let result = [];
		for (let i=0; i<3; i++){
			let msg = Entites.Message.randomMsg(i);
			result.push(msg);
		}
		Command.sendTo(from, new API.Response('history',result));
	},
	contacts: function(payload){
		let from = payload.from;
		let aContacts = [];
		let online = {};
		Command.session.getOnlineFriendsGuids(from).forEach(guid => {
			online[guid] = 1;
		});

		Data.Users.getAll().forEach(o => {
			if (o.guid==from){ return; }
			let oContact = new Entites.Contact(o.guid, o.name, '', online[o.guid] ? 1: 0);
			aContacts.push(oContact.toJSON());
		});
		Command.sendTo(from, new API.Response('contacts',aContacts));
	},
	roster: function(payload){
		const from = payload.from;
		const oModtimes = payload.modtimes;
		const modtime = oModtimes.roster;

		let oRoster = new Entites.Roster();

		// Load messages
		let aMessages = [];
		for (let i=0; i<1; i++){
			let oMsg = Entites.Message.randomMsg(i);
			aMessages.push(oMsg.toJSON());
		}
		// ---

		let online = {};
		Command.session.getOnlineFriendsGuids(from).forEach(guid => {
			online[guid] = 1;
		});

		let aContacts = [];
		Data.Users.getAll().forEach(o => {
			if (o.guid==from){ return; }
			if (!modtime) {
				let oContact = new Entites.Contact(o.guid, o.name, '', online[o.guid] ? 1: 0);
				aContacts.push(oContact.toJSON());
			}
			oRoster.add(o.guid,0);
		});

		let aGroups = [];
		Data.Groups.getAll().forEach(o => {
			if (!modtime) {
				let oGroup = new Entites.Group(o.id, o.name, o.avatar, o.count);
				aGroups.push(oGroup.toJSON());
			}
			oRoster.add('#'+o.id,1);
		});

		let result = {
			contacts: aContacts,
			groups: aGroups,
			roster: oRoster.toJSON()
		};
		if (aContacts.length) {
			result.contacts = aContacts;
		}
		if (aGroups.length) {
			result.groups = aGroups;
		}
		if (aMessages.length) { // Load unread messages
			result.messages = aMessages;
		}

		var response = new API.Response('roster',result);
		Command.sendTo(from, response);

		return true;
	}
};

const AsyncCmd = {
	/**
	 *
	 * {cmd:'typing', payload: {corr:'string'}},
	 * @param o
	 */
	typing: function(o){
		if (!o || !o.corr) return;
		let from = new Entites.Correspondent(o.from);
		let corr = new Entites.Correspondent(o.corr);
		Command.sendTo(corr.id, new API.Response('typing',{corr:from.toString()}));
	},
	/**
	 *
	 * @param payload
	 * {cmd:'msg', payload: {
	 *		corr: 'Correspondent', // 'guid' or '#group_id'
	 *		text: 'string', // message text
	 *		flags: 'number', // [bitmask]
	 *		attach: '[object]' // attach object, optional
	 *	}},
	 */
	msg: function(o){
		if (!o || !o.corr) return;
		let corr = new Entites.Correspondent(o.corr);
		let tm = (Date.now()/1000)^0;
		let atime = new uInt64(0,tm);
		let msg = new Entites.Message(o.from,o.text,atime.toString()).toJSON();

		Command.sendTo(corr.id, new API.Response('msg',msg));
	}
};

const SyncCmd = {
	/**
	 * Init client session
	 * @param {object} payload
	 * {
	 *	'messages':'number', // 0 or modtime
	 *	'contacts':'number', // 0 or modtime
	 *	'groups':'number', // 0 or modtime
	 *	'roster':'number' // 0 or modtime
	 * }},
	 */
	init: function(o){ // sync
		logger.info("SyncCmd.init", o);
		if (typeof(o.roster)!=='undefined'){
			let modtime = parseInt(o.roster,10) || 0;
			Command.executeExt('roster',{from:o.from,modtimes:o});
		}
		return true;
	}
};

const Command = {
	init: function(session){
		this.session = session;
	},
	execute: function(from, cmd, payload){
		if (!from || !cmd || !payload || typeof(payload)!='object'){
			return false;
		}
		let is_sync = typeof(payload.req_id)!='undefined' ? 1 : 0;
		payload.from = from;
		payload.is_sync = is_sync;
		var result = 0;
		if (is_sync){
			if (typeof(SyncCmd[cmd])==='function' && SyncCmd[cmd](payload)){
				result = 1;
			}
		}
		else{
			if (typeof(AsyncCmd[cmd])==='function' && AsyncCmd[cmd](payload)){
				result = 2;
			}
		}
		if (!result) {
			logger.warn("Command.execute:", '`' + cmd + '`', 'sync:' + is_sync, 'payload:', payload, 'result:', result);
		}
		return result>0;
	},
	executeExt: function(cmd,payload) {
		if (!cmd || !payload || !payload.from){
			return false;
		}
		var foundCmd = ExtCmd[cmd];
		if (typeof(foundCmd)!='function'){
			logger.warn("Command.execute.ext", cmd, 'is not found!');
			return false;
		}
		setImmediate(function(){
			if (!foundCmd(payload)){
				logger.log("Command.execute.ext", cmd, payload);
			}
		});
		return true;
	},
	sendTo: function(to, response){
		if (!to || !(response instanceof API.Response)){
			logger.warn('sendTo args error');
			return;
		}
		if (!this.session){
			logger.warn('sendTo error','session undefined');
			return;
		}
		logger.info("Response:", response.cmd, response.payload);
		this.session.findByGuid(to).forEach(
			ses => ses.socket.emit(response.cmd, response.payload)
		);
	},
	sendToFriends: function(guid, response){
		if (!guid || !(response instanceof API.Response)){
			logger.warn('sendToFriend args error');
			return;
		}
		if (!this.session){
			logger.warn('sendToFriend error','session undefined');
			return;
		}
		this.session.getOnlineFriends(guid).forEach(
			ses => ses.socket.emit(response.cmd, response.payload)
		);
	}
};

module.exports = Command;