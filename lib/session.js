/**
 * Created by SERG on 16.06.2016.
 */
class SimpleSession {
	constructor(){
		this.store = {};
	}
	add(token,data){
		if (!token || typeof(data)!='object'){
			return false;
		}
		this.store[token] = data;
		return true;
	}
	findByGuid(guid){
		return this.findBy(o => o.guid==guid);
	}
	getOnlineFriends(guid) {
		return this.findBy(o => {
			return o.guid!=guid
		});
	}
	getOnlineFriendsGuids(guid) {
		var oUniqueGuids = {};
		for (let token in this.store) {
			if (this.store[token].guid!=guid) {
				oUniqueGuids[this.store[token].guid] = 1;
			}
		}
		return Object.keys(oUniqueGuids);
	}
	findBy(key,val){
		var result = [];
		if (typeof(key)=='function'){
			for (let token in this.store) {
				if (key(this.store[token])===true) {
					result.push(this.store[token]);
				}
			}
			return result;
		}
		if (typeof(key)=='string' && typeof(val)!='undefined') {
			for (let token in this.store) {
				if (this.store[token][key] == val) {
					result.push(this.store[token]);
				}
			}
		}
		return result;
	}
	findByToken(token) {
		if (!token) return null;
		return this.store[token];
	}
	remove(token){
		let res = null;
		if (token && this.store[token]){
			res = this.store[token];
			delete this.store[token];
		}
		return res;
	}
}

module.exports = SimpleSession;