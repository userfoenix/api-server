// Requsts: type
// 1 - Async request (type=2)
// 2 - Notification (type=0)
// 3 - Request, waiting for response (type=3)
// 4 - Result binded to parent request,(type=1)

var StatusEntity = {
	guid: 'string', // user guid
	online: 'number', // 0-offline, 1-invisible, 2-online
	status: 'string' // additional user status
};

var MsgEntity = {
	corr: 'string', // guid or #group_id
	guid: '[string]', // optional for group messages, means group member guid
	text: 'string', // Message text
	atime: 'string', // 64 bit string
	flags: 'number', // bitmask
	attach: '[object]' // attach object, optional
};

var RosterEntity = {
	corr: 'string', // guid or #group_id
	modtime: 'number' // last activity time
};

var ContactEntity = {
	guid: 'string', // user guid
	name: 'string', // full name
	pht: 'string', // Or binary, depends of implementation
	gender: 'number',
	online: 'number', // 0-offline, 1-invisible, 2-online
	status: 'string' // additional user status
};

var GroupEntity = {
	id: 'string', // Global group ID
	type: 'number', // normal,public,etc.. - default 0
	name: 'string', // group name
	avatar: 'string', // path to avatar
	count: 'number' // cached amount of members
};

var MemberEntity = {
	group_id: '[string]', // group_id if members came separate from group
	guid: 'string', // member guid
	status: 'status' // 0-pending, 1-approved, 2-deleted
};

// Pseudo-sync commands: `req_id` is required in payload, server will send async response marked with this key
// Client developers should handle such behavioure in theirs own way
var aCmds = [
	{cmd:'init',payload:{ // implemented
		'messages':'number', // 0 or modtime
		'contacts':'number', // 0 or modtime
		'groups':'number', // 0 or modtime
		'roster':'number' // 0 or modtime
	}},
	{cmd:'history',  payload: { // required
		corr: 'string',
		req_id: 'number',
	}},
	{cmd:'search', payload: { // required
		corr: 'string',
		req_id: 'number',
		query: '[number]',
		limit: '[number]',
		offset: '[number]',
	}},
	{cmd:'roster', payload: { // has no sense without getting dependencies
		req_id: 'number',
		modtime:'number' // modtime or 0
	}}
];

var aCmdsResponses = [
	{cmd:'history', payload: {
		req_id: 'number',
		corr: 'string',
		result: [MsgEntity]
	}},
	{cmd:'search', payload: { // required
		req_id: 'number',
		corr: 'string',
		query: '[number]',
		result: [MsgEntity]
	}},
	{cmd:'roster', payload: {
		req_id: 'number',
		result: [RosterEntity] // with deps
	}}
];

// Does not require server response, just give some info or async job for server,
// result will be sent as srv->cli notifications, server will not add information about request Id or other data here
var AsyncCmd = [
	{cmd:'typing',  payload: {corr:'string'}},

	// Messages CRUD
	{cmd:'msg', payload: {
		corr: 'string', // 'guid' or '#group_id'
		text: 'string', // message text
		flags: 'number', // [bitmask]
		attach: '[object]' // attach object, optional
	}},
	{cmd:'msg.edit', payload: { // next iteration
		corr: 'string', // 'guid' or '#group_id'
		atime: 'string', // '64 bitstring' // `atime` of the read message
		text: 'string', // message text
		flags: 'number', // [bitmask]
		attach: '[object]' // attach object, optional
	}},
	{cmd:'msg.delete', payload: { // next iteration
		corr: 'string', // 'guid' or '#group_id'
		atime: 'string' // '64 bitstring' // `atime` of the read message
	}},
	{cmd:'msg.read', payload: { // next iteration
		corr: 'string', // 'guid' or '#group_id'
		atime: '64 bitstring' // `atime` of the read message
	}}
	// ---
];

// Server push-messages, haven't contain any information about own triggers
var aNotifications = [
	{cmd:'inbox', type:'push', payload:{
		result: [MsgEntity]
	}},
	{cmd:'typing', payload: {
		result: {
			corr: 'string', // 'guid' or '#group_id'
			guid: '[string]' // Optional, for groups only
		}
	}},

	{cmd:'contact', modtime:'number', payload: [ContactEntity]},
	{cmd:'status', modtime:'number', payload: [StatusEntity]},

	{cmd:'group', modtime:'number', payload: [GroupEntity]},
	{cmd:'group.update', modtime:'number', payload: {id:'string'}},
	{cmd:'group.member', modtime:'number', payload: {act:'add|del',guid:'string'}}
];