import React from 'react';
import ReactDOM from 'react-dom';
import {newToken, reqId} from './utils';
import MsgList from './components/MsgList.jsx';
import ContactList from './components/ContactList.jsx';

const config = {
	guid: 256, //$("#root").data('guid'),
	username: 'userfoenix' //$("#root").data('username') || 'userfoenix',
};

var socket = io('/?token='+newToken()+'&guid='+config.guid+'&username='+config.username);
socket.on('connect', function(msg){
	console.log("connect", msg);
	socket.emit('init', {messages:0, contacts:0, groups:0, roster:0, req_id:reqId()});
});
socket.on('contact', function(msg){
	console.log("contact", msg);
});
socket.on('group', function(msg){
	console.log("group", msg);
});
socket.on('roster', function(msg){
	console.log("roster", msg);
});
socket.on('status', function(msg){
	console.log("status", msg);
});

socket.on('msg', function(msg){
	if (!msg || !msg.result){
		return;
	}
	var o = msg.result;
	console.log("msg", msg);
	//$('#messages').append($('<li class="">').text(o.text));
});

socket.on('history', function(msg){
	console.log("history", msg);
});

class App extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			active: 1
		}
	}
	sendMessage(){
		console.log('Send');
	}
	componentDidMount(){
		console.log('Rendered');
		var el = this.refs['messages'];
		console.log("el", el);
	}
	render() {
		return <table className="layout" height="100%" width="100%" cellPadding="0" cellSpacing="0">
			<tbody>
			<tr>
				<td colSpan="2">
					Menu bar
				</td>
			</tr>
			<tr>
				<td width="200" className="contacts">
					<ContactList active_corr={this.state.active} />
				</td>
				<td>
					<div className="messages">
						<MsgList user="Userfoenix"/>
					</div>
					<div className="messages-footer">
						<div className="text">
							<textarea></textarea>
						</div>
						<div className="text-send">
							<button onClick={this.sendMessage}>Send</button>
						</div>
					</div>
				</td>
			</tr>
			</tbody>
		</table>
	}
}

ReactDOM.render(
	<App />,
	document.getElementById('app')
);