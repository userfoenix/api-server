export function newToken(o = {}){
	const opts = Object.assign({
		path: 'http://localhost/',
		pref: 'unet',
		token_ttl: 30*60, // 30m in
		refresh_token_ttl: 24*60*60, // 1 day
	}, o);
	console.log("opts", opts);
	var cur_time = (Date.now()/1000) | 0; // time in sec
	var token_expired = cur_time+opts.token_ttl;
	var refresh_token_expired = cur_time+opts.token_ttl;
	return opts.pref+'_'+token_expired+'_'+refresh_token_expired;
}

export function reqId(){
	return (Date.now()/1000) | 0; // time in sec
}