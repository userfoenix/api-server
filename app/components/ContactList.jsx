import React from 'react';
import Contact from './Contact.jsx';

class ContactList extends React.Component {
	constructor(props) {
		super(props);
		this.state = {items: [
			{corr: 1, name: 'Test'},
			{corr: 2, name: 'Vasya 1'},
			{corr: 3, name: 'Ola 2'},
			{corr: 4, name: 'Rest 3'}
		]};
	}
	render() {
		const active_corr = this.props.active_corr;
    	return <ul className="contact-list">
				{
					this.state.items.map((o) => {
						let Letter = o.name[0].toUpperCase();
						return <Contact key={o.corr} corr={o.corr} name={o.name} avatar={Letter}/>
					})
				}
			</ul>
	}
}
export default ContactList;

