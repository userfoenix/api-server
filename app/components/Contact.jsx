import React from 'react';

class Contact extends React.Component {
	constructor(props) {
		super(props);
	}
	render() {
		const active_corr = this.props.active_corr;
		return <li className="contact"><a>{this.props.name}</a></li>
	}
}
export default Contact;

