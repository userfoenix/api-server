import React from 'react';
import Msg from './Msg.jsx';

export default class MsgList extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			items: [
				{id: 1, text: 'Text1', corr: 768, name: 'Vasua'},
				{id: 2, text: 'Text3', corr: 256, name: 'Userfoenix'}
			]
		}
	}
	render() {
		return <div className="messages-wrap">
			<h2>{this.props.user}</h2>
			{
				this.state.items.map((o,index) => {
					let Letter = o.name[0].toUpperCase();
					return <Msg
							key={o.id}
							avatar={Letter}
							name={o.name}
							corr={o.corr}
							text={<p>{o.text}</p>}
						/>
					})
			}
			</div>
	}
}

